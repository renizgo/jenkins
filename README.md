# easynvest.jenkins

Implementar o Jenkins com uma instalação automatizada.

# Instalação

Faça o clone do repositório, usando seu usuário e senha:

```
git clone https://bitbucket.org/easynvest/easynvest.jenkins.git
```

Entre no diretório e dê permissão de execução no script de instalação:
```
$ chmod +x install.sh
```

Certifique-se de que você reiniciou o servidor, para que você consiga fazer os comandos do Docker sem o uso do sudo:

```
docker --version
```

Entre no diretório do repositório e suba o serviços com o o comando:

```
$ docker-compose up -d
```




