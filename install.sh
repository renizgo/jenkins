#!/bin/bash

# Verifying instalation of Git
git version > /dev/null
echo "----------"
if [ $? -eq 0 ]; then
  echo "Git OK"
  VERSION=`git version`
  echo "Version of Git is: $VERSION"
else
  echo "Installing Git"
  sudo apt install -y git
  VERSION=`git version`
  echo "Git was installed in version: $VERSION"
fi

# Verifying instalation of wget
which wget > /dev/null
echo "----------"
if [ $? -eq 0 ]; then
  echo "Wget OK"
  LOCATE=`which wget`
  echo "Wget is installed in: $LOCATE"
else
  echo "Installing wget"
  sudo apt install -y wget
  LOCATE=`which wget`
  echo "Wget was installed in: $LOCATE"
fi

# Verifying instalation of curl
which curl > /dev/null
echo "----------"
if [ $? -eq 0 ]; then
  echo "Curl OK"
  LOCATE=`which curl`
  echo "Curl is installed in: $LOCATE"
else
  echo "Installing Curl"
  sudo apt install -y wget
  LOCATE=`which curl`
  echo "Curl was installed in: $LOCATE"
fi


# Verifying instalation of Docker
echo "verifying Docker installation, please insert Sudo password"
sudo docker --version 1> /dev/null 2> /dev/stdout
echo "----------"
if [ $? -ne 0 ]; then
  echo "Docker OK"
  echo "Docker is installed in: "
else
  echo "Installing Docker"
  sudo apt-get update -y
  sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
  sudo apt-get update -y
  sudo apt-get install -y docker-ce
  echo "----------"
  sudo docker --version
  echo "----------"
  echo "Above appears docker version? yes or no"
  sudo apt install -y docker-compose
  sudo docker-compose --version
  echo "----------"
  echo "Above appears docker version? Yes or No"
fi

echo "Configuring Docker permission for use Docker without SUDO"

groupadd docker
usermod -aG docker ubuntu
mkdir /home/"ubuntu"/.docker
chown "ubuntu":"ubuntu" /home/"ubuntu"/.docker -R
chmod g+rwx "/home/ubuntu/.docker" -R

sudo ufw disable

echo "Create docker networking with jenkins name"

sudo docker network create jenkins

cd ~/easynvest.jenkins/
sudo docker build -t easynvest.jenkins:0.1.0 .

sudo find /opt/jenkins/easynvest.jenkins/jenkins_home/ -type d -exec chmod 777  "{}" \;
sudo find /opt/jenkins/easynvest.jenkins/jenkins_home/ -type f -exec chmod 666  "{}" \;

sudo find /opt/jenkins/easynvest.jenkins/jenkins_backup/ -type d -exec chmod 777  "{}" \;
sudo find /opt/jenkins/easynvest.jenkins/jenkins_backup/ -type f -exec chmod 666  "{}" \;

echo "Configuring Hour Timezone"
sudo timedatectl set-timezone America/Sao_Paulo

echo "@reboot /usr/bin/docker-compose -f /opt/jenkins/easynvest.jenkins/docker-compose.yaml  up -d" | crontab -
echo "Do you wish restart you server?"
sudo reboot
